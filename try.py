import requests as rq
#from typing import Self
import random
DEBUG = False

class MMBot:
    words = [word.strip() for word in open("words.txt")]
    mm_url = "https://we6.talentsprint.com/wordle/game/"
    register_url = mm_url + "register"
    creat_url = mm_url + "create"
    guess_url = mm_url + "guess"

    def __init__(self, name: str):
        
        self.session = rq.session()
        register_dict = {"mode": "wordle", "name": name}
        reg_resp = self.session.post(MMBot.register_url, json=register_dict)
        self.me = reg_resp.json()['id']
        creat_dict = {"id": self.me, "overwrite": True}
        self.session.post(MMBot.creat_url, json=creat_dict)

        self.choices = [w for w in MMBot.words[:]]
        random.shuffle(self.choices)

    def play(self) -> str:
        def post(choice: str) -> tuple[int, bool]:
            guess = {"id": self.me, "guess": choice}
            response = self.session.post(MMBot.guess_url, json=guess)
            rj = response.json()
            right = rj["feedback"]
            status = "win" in rj["message"]
            return right, status

        choice = random.choice(self.choices)
        self.choices.remove(choice)
        right, won = post(choice)
        tries = [f'{choice}:{right}']

        while not won:
            if DEBUG:
                print(choice, right, self.choices[:10])
            self.update(choice, right)
            choice = random.choice(self.choices)
            self.choices.remove(choice)
            right, won = post(choice)
            tries.append(f'{choice}:{right}')
        print("Secret is", choice, "found in", len(tries), "attempts")
        print("Route is:", " => ".join(tries))

    def update(self, choice: str, right: list[str]):

        def check_for_green(word: str,g:dict):
            return all(word[i] == g[i] for i in g.keys())
               # for i in g:
               #     if word[i] != g[i]:
               #         return False
               # return True

        def check_for_red(word,r):
            #probable_words = set(g.values()) | set(y.values())
            return all(i not in r for i in word) 
            #for i in word:
            #    if i not in g.values() or i in y.values():
            #       return True
                        
        #def common(choice: str, word: str):
        g = {}
        #y = {}
        r = []
        for pos,char in enumerate(right):
            if char == 'G':
                g[pos] = choice[pos]
            #elif char == 'Y':
            #    y[pos] = choice[pos]
            elif char == 'R':
                r.append(choice[pos])
         #   if word not in r.values():
         #       if check_for_green(word,g):
         #          
         #           return True

        self.choices = [w for w in self.choices if check_for_red(w,r) & check_for_green(w,g)]


game = MMBot("avni")
game.play()

import requests as rq
from typing import Self
import random
DEBUG = False

class wordleBot:
    words = [word.strip() for word in open("words.txt")]
    wordle_url = "https://we6.talentsprint.com/wordle/game/"
    register_url = wordle_url + "register"
    start_url = wordle_url + "start"
    guess_url = wordle_url + "guess"

    def __init__(self: Self, name: str):
        

        self.session = rq.session()
        register_dict = {"name": name}
        reg_resp = self.session.post(wordleBot.register_url, json=register_dict)
        self.me = reg_resp.json()['id']
        creat_dict = {"id": self.me, "overwrite": True}
        self.session.post(wordleBot.creat_url, json=creat_dict)

        self.choices = [w for w in wordleBot.words[:]]
        random.shuffle(self.choices)

    def play(self: Self) -> str:
        def post(choice: str) -> tuple[int, bool]:
            guess = {"id": self.me, "guess": choice}
            response = self.session.post(wordleBot.guess_url, json=guess)
            rj = response.json()
            green = int(rj["feedback"]["green"])
            yellow = int(rj["feedback"]["yellow"])
            red = int(rj["feedback"]["red"])
            status = "win" in rj["message"]
            return green, yellow, red , status

        choice = random.choice(self.choices)
        self.choices.remove(choice)
        green, yellow, red, won = post(choice)
        tries = [f'{choice}: G{green} Y{yellow} X{red}']

        attempt = 1
        while not won and attempt <6:
            if DEBUG:
                print(choice, green, yellow , red, self.choices[:10])
            self.update(choice, green , yellow ,red)
            choice = random.choice(self.choices)
            self.choices.remove(choice)
            green, yellow, red, won = post(choice)
            tries.append(f'{choice}: G{green} Y{yellow} X{red}')
        print("Secret is", choice, "found in", len(tries), "attempts")
        print("Route is:", " => ".join(tries))
        attempt += 1

    def update(self: Self, choice: str, green: int, yellow: int, grey: int):
        def match_feedback(word: str) -> bool:
            g, y, x = 0, 0, 0
            for i in range(len(choice)):
                if choice[i] == word[i]:
                    g += 1
                elif choice[i] in word:
                    y += 1
                else:
                    x += 1
            return g == green and y == yellow and x == red
        self.choices = [w for w in self.choices if match_feedback(w)]


game = wordleBot("CodeShifu")
game.play()
